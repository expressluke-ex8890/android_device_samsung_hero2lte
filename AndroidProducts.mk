PRODUCT_MAKEFILES := \
	$(LOCAL_DIR)/aosp_hero2lte.mk \
	$(LOCAL_DIR)/euphoria_hero2lte.mk \
	$(LOCAL_DIR)/lineage_hero2lte.mk

COMMON_LUNCH_CHOICES := \
    lineage_hero2lte-userdebug \
    euphoria_hero2lte-userdebug \
	aosp_hero2lte-userdebug

